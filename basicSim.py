import pandas as pd
import helper
import numpy

def transitive_calc_consumption(ds ,clientSrc, numClients, period, op_type):
    ret = 0
    for nClientDest in range(numClients):
        if clientSrc != nClientDest:
            ret = ret + ds[str(clientSrc+1) + op_type + str(nClientDest+1)][period]


    return ret

def transitive_sum_all(ds, numClients, op_type, p=-1):
    ret = 0

    if p == -1:
        for period in range(len(ds)):
            for nclient in range(numClients):
                ret += ds[op_type + str(nclient+1)][period]
        return ret
    
    period = p
    for nclient in range(numClients):
        ret += ds[op_type + str(nclient+1)][period]
    
    return ret


def transitiveEnergy(ds,numClients, fromGridPrice, toGridPrice, transactivePrice):

    #Calculate and distribute transactive energy
    for period in range(len(ds)):
        for nClientGerador in range(numClients):
            genGerador = ds['To grid ' + str(nClientGerador+1)][period]
            consGerador = ds['From grid ' + str(nClientGerador+1)][period]
            
            
            if ds['To grid ' + str(nClientGerador+1)][period] > 0: #todo: from grid??
                
                #Trocas de energia
                for nClientVender in range(numClients):
                    if nClientVender != nClientGerador:
                        genConsumidor = ds['To grid ' + str(nClientVender+1)][period]
                        consConsumidor = ds['From grid ' + str(nClientVender+1)][period]
                        
                        transSellsConsumidor = transitive_calc_consumption(ds, nClientVender,numClients,period,' sells to User ')
                        transBuyConsumidor = transitive_calc_consumption(ds, nClientVender,numClients,period,' consumes from User ')
                        
                        transSellsGerador = transitive_calc_consumption(ds, nClientGerador,numClients,period,' sells to User ')
                        transBuyGerador = transitive_calc_consumption(ds, nClientGerador,numClients,period,' consumes from User ')

                        gerador_what_can_sell = genGerador - transSellsGerador

                        if consConsumidor - genConsumidor - transBuyConsumidor > 0 and gerador_what_can_sell > 0 :
                            
                            if consConsumidor - genConsumidor - transBuyConsumidor > gerador_what_can_sell:
                                ds[str(nClientGerador+1) + ' sells to User ' + str(nClientVender+1)][period] =  gerador_what_can_sell
                                ds[str(nClientVender+1) + ' consumes from User ' + str(nClientGerador+1)][period] = gerador_what_can_sell
                            else:
                                ds[str(nClientGerador+1) + ' sells to User ' + str(nClientVender+1)][period] =  consConsumidor - genConsumidor - transBuyConsumidor 
                                ds[str(nClientVender+1) + ' consumes from User ' + str(nClientGerador+1)][period] = consConsumidor - genConsumidor - transBuyConsumidor
                            
                            tan_e = ds[str(nClientVender+1) + ' consumes from User ' + str(nClientGerador+1)][period] /1000*transactivePrice[period]
                            ds[str(nClientVender+1) + ' payed to User ' + str(nClientGerador+1)][period] += tan_e
                            ds[str(nClientGerador+1) + ' payed to User ' + str(nClientVender+1)][period] += tan_e*-1

            #Acertos com grid:
            
            ds['w/smartGrid To grid ' + str(nClientGerador+1)][period] = genGerador - transitive_calc_consumption(ds, nClientGerador,numClients,period,' sells to User ')
            ds['w/smartGrid From grid ' + str(nClientGerador+1)][period] = consGerador - transitive_calc_consumption(ds, nClientGerador,numClients,period,' consumes from User ')
            ds['w/smartGrid Earned from grid ' + str(nClientGerador+1)][period] = ds['w/smartGrid To grid ' + str(nClientGerador+1)][period]/1000*toGridPrice[period]

            ds['w/smartGrid Payed to grid ' + str(nClientGerador+1)][period] = ds['w/smartGrid From grid ' + str(nClientGerador+1)][period]/1000*fromGridPrice[period]
            ds['w/smartGrid Earned from localgrid ' + str(nClientGerador+1)][period] = transitive_calc_consumption(ds, nClientGerador,numClients,period,' sells to User ')/1000*transactivePrice[period]
            ds['w/smartGrid Payed to localgrid ' + str(nClientGerador+1)][period] = transitive_calc_consumption(ds, nClientGerador,numClients,period,' consumes from User ')/1000*transactivePrice[period]
        

        # Calculo Periodo a Periodo (Instantaneo):

        ds['Grid payed inst'][period] = transitive_sum_all(ds, numClients ,'To grid ',p=period)/1000*toGridPrice[period]
        ds['Grid earned inst'][period] = transitive_sum_all(ds, numClients ,'From grid ',p=period)/1000*fromGridPrice[period]
        ds['Grid balance inst'][period] = ds['Grid earned inst'][period] - ds['Grid payed inst'][period] 

        ds['SmartGrid Grid earned inst'][period] = transitive_sum_all(ds, numClients ,'w/smartGrid Payed to grid ',p=period)
        ds['SmartGrid Grid payed inst'][period] = transitive_sum_all(ds, numClients , 'w/smartGrid Earned from grid ', p=period)
        ds['SmartGrid Grid balance inst'][period] = ds['SmartGrid Grid earned inst'][period] - ds['SmartGrid Grid payed inst'][period] 
        ds['SmartGrid Local Grid Earned inst'][period] = transitive_sum_all(ds, numClients , 'w/smartGrid Earned from localgrid ', p=period)
        ds['SmartGrid Local Grid Payed inst'][period] = transitive_sum_all(ds, numClients , 'w/smartGrid Payed to localgrid ', p=period)
        ds['SmartGrid Local Grid Balance inst'][period] = ds['SmartGrid Local Grid Earned'][period] - ds['SmartGrid Local Grid Payed'][period]

    #Total:
    ds['SmartGrid Grid earned'] = transitive_sum_all(ds, numClients ,'w/smartGrid Payed to grid ')
    ds['SmartGrid Grid payed'] = transitive_sum_all(ds, numClients , 'w/smartGrid Earned from grid ')
    ds['SmartGrid Grid balance'] = ds['SmartGrid Grid earned'] - ds['SmartGrid Grid payed'] 
    ds['SmartGrid Local Grid Earned'] = transitive_sum_all(ds, numClients , 'w/smartGrid Earned from localgrid ')
    ds['SmartGrid Local Grid Payed'] = transitive_sum_all(ds, numClients , 'w/smartGrid Payed to localgrid ')
    ds['SmartGrid Local Grid Balance'] = ds['SmartGrid Local Grid Earned'] - ds['SmartGrid Local Grid Payed'] 

    return ds

def runCommonDay(ds, numClients, fromGridPrice, toGridPrice, smartgrid, transactivePrice=[]):
    
    ds_res = helper.generate_dataframe(ds, True)
    money2grid = 0
    money2community = 0
    total_cons = 0
    total_gen = 0

    for nClient in range(numClients):
        toGrid = []
        fromGrid = []
        earnGrid = []
        payGrid = []


        for period in range(len(ds['Consumption ' + str(nClient+1)])):
            cons = ds['Consumption ' + str(nClient+1)][period]
            gen = ds['Generation ' + str(nClient+1)][period]
            toGrid.append(gen)
            fromGrid.append(cons)
            earnGrid.append(gen/1000*toGridPrice[period])
            payGrid.append(cons/1000*fromGridPrice[period])

        ds_res['Consumption ' + str(nClient+1)] = ds['Consumption ' + str(nClient+1)]
        ds_res['Generation ' + str(nClient+1)] = ds['Generation ' + str(nClient+1)]
        
        for k in range(numClients):
            if k == nClient:
                ds_res['To grid ' + str(nClient+1)] = toGrid
                ds_res['From grid ' + str(nClient+1)] = fromGrid
                ds_res['Earned from grid ' + str(nClient+1)] = earnGrid
                ds_res['Payed to grid ' + str(nClient+1)] = payGrid

                ds_res['w/smartGrid To grid ' + str(nClient+1)] = 0
                ds_res['w/smartGrid From grid ' + str(nClient+1)] = 0
                ds_res['w/smartGrid Earned from grid ' + str(nClient+1)] = 0.0
                ds_res['w/smartGrid Payed to grid ' + str(nClient+1)] = 0.0
                ds_res['w/smartGrid Earned from localgrid ' + str(nClient+1)] = 0.0
                ds_res['w/smartGrid Payed to localgrid ' + str(nClient+1)] = 0.0
            else:
                ds_res[str(nClient+1) + ' sells to User ' + str(k+1)] = 0
                ds_res[str(nClient+1) + ' consumes from User ' + str(k+1)] = 0
                ds_res[str(nClient+1) + ' payed to User ' + str(k+1)] = 0
                

        money2grid += sum(payGrid)
        money2community += sum(earnGrid)
        total_cons += sum(ds['Consumption ' + str(nClient+1)])
        total_gen += sum(ds['Generation ' + str(nClient+1)])

    ds_res['Grid earned'] = money2grid
    ds_res['Grid payed'] = money2community
    ds_res['Grid balance'] = money2grid - money2community
    ds_res['Community earn'] = money2community
    ds_res['Community payed'] = money2grid
    ds_res['Community balance'] = money2community - money2grid
    ds_res['Community consumption'] = total_cons
    ds_res['Community generation'] = total_gen
    
    return transitiveEnergy(ds_res,numClients,fromGridPrice,toGridPrice,transactivePrice)

def runSelfConsumption(ds, numClients, fromGridPrice, toGridPrice, smartgrid, transactivePrice=[]):
    ds_res = helper.generate_dataframe(ds, True)
    money2grid = 0
    money2community = 0
    total_cons = 0
    total_gen = 0

    for nClient in range(numClients):
        toGrid = []
        fromGrid = []
        earnGrid = []
        payGrid = []

        for period in range(len(ds['Consumption ' + str(nClient+1)])):
            cons = ds['Consumption ' + str(nClient+1)][period]
            gen = ds['Generation ' + str(nClient+1)][period]
            if gen > cons:
                toGrid.append(gen-cons)
                fromGrid.append(0)
                earnGrid.append((gen-cons)/1000*toGridPrice[period])
                payGrid.append(0)
            else:
                toGrid.append(0)
                fromGrid.append(cons-gen)
                earnGrid.append(0)
                payGrid.append((cons-gen)/1000*fromGridPrice[period])

        ds_res['Consumption ' + str(nClient+1)] = ds['Consumption ' + str(nClient+1)]
        ds_res['Generation ' + str(nClient+1)] = ds['Generation ' + str(nClient+1)]
                
        for k in range(numClients):
            if k == nClient:
                ds_res['To grid ' + str(nClient+1)] = toGrid
                ds_res['From grid ' + str(nClient+1)] = fromGrid
                ds_res['Earned from grid ' + str(nClient+1)] = earnGrid
                ds_res['Payed to grid ' + str(nClient+1)] = payGrid
                ds_res['w/smartGrid To grid ' + str(nClient+1)] = 0.0
                ds_res['w/smartGrid From grid ' + str(nClient+1)] = 0.0
                ds_res['w/smartGrid Earned from grid ' + str(nClient+1)] = 0.0
                ds_res['w/smartGrid Payed to grid ' + str(nClient+1)] = 0.0
                ds_res['w/smartGrid Earned from localgrid ' + str(nClient+1)] = 0.0
                ds_res['w/smartGrid Payed to localgrid ' + str(nClient+1)] = 0.0
            else:
                ds_res[str(nClient+1) + ' sells to User ' + str(k+1)] = 0.0
                ds_res[str(nClient+1) + ' consumes from User ' + str(k+1)] = 0.0
                ds_res[str(nClient+1) + ' payed to User ' + str(k+1)] = 0.0


        money2grid += sum(payGrid)
        money2community += sum(earnGrid)
        total_cons += sum(ds['Consumption ' + str(nClient+1)])
        total_gen += sum(ds['Generation ' + str(nClient+1)])

    ds_res['Grid earned'] = money2grid
    ds_res['Grid payed'] = money2community
    ds_res['Grid balance'] = money2grid - money2community
    ds_res['Community earn'] = money2community
    ds_res['Community payed'] = money2grid
    ds_res['Community balance'] = money2community - money2grid
    ds_res['Community consumption'] = total_cons
    ds_res['Community generation'] = total_gen
    return transitiveEnergy(ds_res,numClients,fromGridPrice,toGridPrice,transactivePrice)

def runSelfConsumption_only(ds, numClients, fromGridPrice,smartgrid, transactivePrice=[]):
    ds_res = helper.generate_dataframe(ds, True)
    money2grid = 0
    money2community = 0
    total_cons = 0
    total_gen = 0

    for nClient in range(numClients):
        toGrid = []
        fromGrid = []
        earnGrid = []
        payGrid = []

        for period in range(len(ds['Consumption ' + str(nClient+1)])):
            cons = ds['Consumption ' + str(nClient+1)][period]
            gen = ds['Generation ' + str(nClient+1)][period]
            if gen > cons:
                toGrid.append(gen-cons)
                fromGrid.append(0)
                earnGrid.append(0)
                payGrid.append(0)
            else:
                toGrid.append(0)
                fromGrid.append(cons-gen)
                earnGrid.append(0)
                payGrid.append((cons-gen)/1000*fromGridPrice[period])
        
        
        ds_res['Consumption ' + str(nClient+1)] = ds['Consumption ' + str(nClient+1)]
        ds_res['Generation ' + str(nClient+1)] = ds['Generation ' + str(nClient+1)]
        for k in range(numClients):
            if k == nClient:
                ds_res['To grid ' + str(nClient+1)] = toGrid
                ds_res['From grid ' + str(nClient+1)] = fromGrid
                ds_res['Earned from grid ' + str(nClient+1)] = earnGrid
                ds_res['Payed to grid ' + str(nClient+1)] = payGrid

                ds_res['w/smartGrid To grid ' + str(nClient+1)] = 0
                ds_res['w/smartGrid From grid ' + str(nClient+1)] = 0
                ds_res['w/smartGrid Earned from grid ' + str(nClient+1)] = 0.0
                ds_res['w/smartGrid Payed to grid ' + str(nClient+1)] = 0.0
                ds_res['w/smartGrid Earned from localgrid ' + str(nClient+1)] = 0.0
                ds_res['w/smartGrid Payed to localgrid ' + str(nClient+1)] = 0.0
            else:
                ds_res[str(nClient+1) + ' sells to User ' + str(k+1)] = 0
                ds_res[str(nClient+1) + ' consumes from User ' + str(k+1)] = 0
                ds_res[str(nClient+1) + ' payed to User ' + str(k+1)] = 0

        money2grid += sum(payGrid)
        money2community += sum(earnGrid)
        total_cons += sum(ds['Consumption ' + str(nClient+1)])
        total_gen += sum(ds['Generation ' + str(nClient+1)])

    ds_res['Grid earned'] = money2grid
    ds_res['Grid payed'] = money2community
    ds_res['Grid balance'] = money2grid - money2community
    ds_res['Community earn'] = money2community
    ds_res['Community payed'] = money2grid
    ds_res['Community balance'] = money2community - money2grid
    ds_res['Community consumption'] = total_cons
    ds_res['Community generation'] = total_gen
    return transitiveEnergy(ds_res,numClients,fromGridPrice,numpy.zeros(len(ds['Consumption ' + str(nClient+1)])),transactivePrice)